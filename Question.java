public class Question<maReponseParameter> {

    public String maQuestion;
    public String maReponse;

    public Question(String maQuestionParameter, String maReponseParameter) {
        this.maQuestion = maQuestionParameter;
        this.maReponse = maReponseParameter;
    }

    public String getmaQuestion() {
        return this.maQuestion;
    }

    public String getmaReponse() {
        return this.maReponse;
    }
    }
